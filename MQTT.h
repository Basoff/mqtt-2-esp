#include <PubSubClient.h>

PubSubClient mqtt_cli(wifiClient);

/*bool check_LED(char t, int val){
 * n = atoi(t);
  if (n == 48){
    if (val < 130)
      digitalWrite(LED_PIN, HIGH);
    else
      digitalWrite(LED_PIN, LOW);
  }
  else if (n == 49){
    if (val < 20)
      digitalWrite(LED_PIN, HIGH);
    else
      digitalWrite(LED_PIN, LOW);
  }
}*/


void callback(char *topic, byte *payload, unsigned int length) {
    String callback_res = "";
    char t = (char)payload[0];
    for (int i = 1; i < length; i++) {
        callback_res = callback_res + (char)payload[i];
      }
    int num = callback_res.toInt();
    if (t == '0'){
      if (num < 130) digitalWrite(LED_PIN, HIGH);
      else digitalWrite(LED_PIN, LOW);
    }
    else if (t == '1'){
      if (num < 20) digitalWrite(LED_PIN, HIGH);
      else digitalWrite(LED_PIN, LOW);
    }
}

void MQTT_init(){
  mqtt_cli.setServer(mqtt_broker, mqtt_port);
  mqtt_cli.setCallback(callback);
  while (!mqtt_cli.connected()) {
      String client_id = "esp8266-" + String(WiFi.macAddress());
      Serial.print("The client " + client_id);
      Serial.println(" connects to the public mqtt broker\n");
      if (mqtt_cli.connect(client_id.c_str())){
          Serial.println("MQTT Connected");
      } else {
          Serial.print("failed with state ");
          Serial.println(mqtt_cli.state());
          delay(2000);
      }
  }  
}
