#include <ESP8266WebServer.h>

ESP8266WebServer server(80);    

void handleRoot() {                         
  server.send(200, 
              "text/html", 
              "<form action=\"/SWITCH\" method=\"POST\"><input type=\"submit\" value=\"SWITCH\"></form>");
}

void handleSWITCH() {                          
  state = !state;
  server.sendHeader("Location","/"); // redirection to keep button on the screen
  server.send(303);
}

void handleNotFound(){
  server.send(404, "text/plain", "404: Not found"); 
}

void server_init() {
  server.on("/", HTTP_GET, handleRoot);     
  server.on("/SWITCH", HTTP_POST, handleSWITCH);  
  server.onNotFound(handleNotFound);        

  server.begin();                          
  Serial.println("HTTP server started");    
}
