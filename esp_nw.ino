#include "Config.h"
#include "WIFI.h"
#include "Server.h"
#include "MQTT.h"

#define TRIG_PIN 12
#define ECHO_PIN 13
#define LIGHT_PIN A0
String value;

void setup(void){
  Serial.begin(9600);
  WIFI_init(false);
  delayMicroseconds(50);
  server_init();
  delayMicroseconds(50);
  MQTT_init();
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  mqtt_cli.publish("esp8266/state", "hello emqx");
  mqtt_cli.subscribe("esp8266/data");
}

String read_distance(){
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIG_PIN, HIGH);

  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  long duration = pulseIn(ECHO_PIN, HIGH);
  
  int cm = (duration / 2) / 29.1;

  /*Serial.print("Расстояние до объекта: ");
  Serial.print(cm);
  Serial.println(" см.");*/
  return (String) cm;
}

String read_light(){
  int val = analogRead(LIGHT_PIN);
  delayMicroseconds(50);
  /*Serial.print("Освещение: ");
  Serial.println(val);*/
  return (String) val;
}
/* 
 res = ""
 while (Serial.available()){
    res += Serial.read();
 }
 Serial.println(res);
 */
void loop(void){
  server.handleClient();
  mqtt_cli.loop();
  if (state){
    value = "1" + read_distance();
  }
  else{
    value = "0" + read_light();
  }
  Serial.println(value);
  delay(250);
}
